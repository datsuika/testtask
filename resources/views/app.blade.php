<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <title>Test Task</title>
    </head>
    <body>
        <div class="container pt-4">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mb-3">{{$error}}</div>
                @endforeach
            @endif
            @if (session('success')) 
                <div class="alert alert-success mb-3">{{ session('success') }}</div>
            @endif
            <div class="mb-5">                               
                <form action="{{ route('store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="dataset" class="form-label">File</label>
                        <input class="form-control" type="file" id="dataset" name="dataset">
                    </div>
                    <button type="submit" class="btn btn-primary">Import</button>
                </form>
            </div><br />

            <div id="error"></div>
            <form action="{{ route('index') }}" onsubmit="return validate()"  method="get">
                <div class="row mb-4">  
                    <div class="col-auto mb-2 pr-0">
                        <select name="category" id="category" class="form-select">
                            <option value="">All categories</option>
                            @if (!empty($categories))
                                @foreach ($categories as $category)
                                    <option 
                                        value="{{ $category['category'] }}"
                                        {{ old('category') == $category['category'] ? 'selected' : '' }}
                                    >{{ $category['category'] }}</option>
                                @endforeach    
                            @endif 
                        </select>
                    </div>
                    <div class="col-auto mb-2 pr-0">
                        <select name="gender" id="gender" class="form-select">
                            <option value="">All genders</option>
                            @if (!empty($genders))
                                @foreach ($genders as $gender)
                                    <option 
                                        value="{{ $gender }}" 
                                        {{ old('gender') == $gender ? 'selected' : '' }}
                                    >{{ $gender }}</option>
                                @endforeach    
                            @endif 
                        </select>
                    </div>
                    <div class="col-auto mb-2 pr-0">
                        <input type="date" 
                            name="birthdate" 
                            id="birthdate" 
                            class="form-control" 
                            value="{{ old('birthdate') }}">
                    </div>   
                    <div class="col-auto mb-2 pr-0">
                        <input type="number" 
                            name="age" 
                            id="age" 
                            min="0"
                            style="width: 100px;"
                            placeholder="Age" 
                            class="form-control" 
                            value="{{ old('age') }}">
                    </div> 
                    <div class="col-auto mb-2 pr-0">
                        <input type="number" 
                            name="age_to" 
                            id="age_to" 
                            min="0"
                            style="width: 100px;"
                            placeholder="Age to" 
                            class="form-control" 
                            value="{{ old('age_to') }}">
                    </div> 
                    <div class="col-auto mb-2 pr-0">
                        <button type="submit" name="action" value="filter" class="btn btn-primary">Filter</button>
                    </div>
                    @if (request()->hasAny(['category', 'gender', 'birthdate', 'age']))                       
                    <div class="col-auto mb-2 pr-0">
                        <button type="submit" name="action" value="export" class="btn btn-dark">Export</button>
                    </div>       
                    @endif  
                </div>
            </form>


            <table class="table table-sm mb-5">
                <thead>
                <tr>
                    <th scope="col">Category</th>
                    <th scope="col">Firstname</th>
                    <th scope="col">Lastname</th>
                    <th scope="col">Email</th>
                    <th scope="col">Gender</th>
                    <th scope="col">BirthDate</th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($clients))
                    @foreach ($clients as $client)
                    <tr>
                        <td>{{ $client->category }}</th>
                        <td>{{ $client->firstname }}</td>
                        <td>{{ $client->lastname }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->gender }}</td>
                        <td>{{ $client->birth_date }}</td>                   
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>

            <div class="mb-5">{{ $clients->links() }}</div>
        </div>
        
<script>
function validate() {
    let age    = document.getElementById('age').value;
    let age_to = document.getElementById('age_to').value;
 
    if (age_to.value.length !== 0) { 
        if (age > age_to) {
            let eDiv = document.createElement('div');
            eDiv.className = 'alert alert-danger';
            eDiv.textContent = 'first age should be less';

            document.getElementById('error').appendChild(eDiv);
       
            return false;
        }
    }
    return true;
}
</script>
</body>
</html>
