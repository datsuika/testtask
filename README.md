## Installation

Clone repository:

```sh
git clone https://gitlab.com/datsuika/testtask.git
```

Install dependencies:

```sh
composer install
```

Enter your database login information, including the database name, username, and password, after opening the .env file:

```sh
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<DATABASE NAME>
DB_USERNAME=<DATABASE USERNAME>
DB_PASSWORD=<DATABASE PASSWORD>
```

Make migration

```sh
php artisan migrate
```

