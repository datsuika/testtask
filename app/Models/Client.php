<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {
    use HasFactory;

    protected $table = 'clients';

    public $timestamps = false;

    protected $fillable = [
        'category',
        'firstname',
        'lastname',
        'email',
        'gender',
        'birth_date'
    ];   


    public function scopeFilterCategory($query, $category) {
        if ($category) {
            return $query->where('category', $category);
        }
    }
    public function scopeFilterGender($query, $gender) {
        if ($gender) {
            return $query->where('gender', $gender);
        }
    }
    public function scopeFilterBirthday($query, $date) {
        if ($date) {
            return $query->where('birth_date', $date);
        }
    }
    public function scopeFilterAge($query, $age, $ageTo = null) {
        if ($age) {
            if ($ageTo) {
                return $query->whereBetween('birth_date', [
                    $this->freshTimestamp()->subYears($ageTo)->format('Y-m-d'), 
                    $this->freshTimestamp()->subYears($age)->format('Y-m-d')
                ]); 
            } else {
                return $query->whereYear('birth_date', date('Y') - $age);
            }
        }
    }


    public function scopeAgedBetween($query, $start, $end = null) { 
        if (is_null($end)) {
            $end = $start;
        } 

        $start = $this->freshTimestamp()->subYears($start)->format('Y-m-d');  
        $end   = $this->freshTimestamp()->subYears($end)->format('Y-m-d');    

        return $query->whereBetween('birth_date', [$end, $start]);
    }


    public function getBirthDateAttribute($value) {
        return date('d.m.Y', strtotime($value));
    }   
}
