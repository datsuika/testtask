<?php
namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\LazyCollection;



class HomeController {
 
	public function index(Request $request) {    
		$query = Client::select('*');

		switch ($request->input('action')) {
			case 'filter':
				$query
					->filterCategory($request->category)
					->filterGender($request->gender)
					->filterBirthday($request->birthdate)
					->filterAge($request->age, $request->age_to)
					->paginate(20)
					->withQueryString();
            break;
	        case 'export':
	        	return $this->export($request);
	        break;
	    }	

        $request->flash();

		return view('app', [
            'clients'    => $query->paginate(20)->withQueryString(),
            'categories' => Client::distinct()->orderBy('category')->get(['category'])->toArray(),
            'genders'    => ['male', 'female']
        ]);  
	}	




	public function store(Request $request) {  
 		$request->validate([
            'dataset' => 'file|required|mimes:txt'
        ]); 

		if ($request->has('dataset')) {

			if ($file = $request->file('dataset')) {  
				$name = $file->getClientOriginalName();  
				$file->move('temp', $name);  
			}  

			LazyCollection::make(function () {
		      	$handle = fopen(public_path('temp/dataset.txt'), 'r');
		      
			    while ($line = fgetcsv($handle)) {
			        yield $line;
			    }
		      	fclose($handle);
		    })
		    ->skip(1)
		    ->chunk(1000)
		    ->each(function (LazyCollection $chunk) {
		      	$records = $chunk->map(function ($row) {
 		        	return [
		            	'category'   => $row[0],
		            	'firstname'  => $row[1],
		            	'lastname'   => $row[2],
		            	'email'      => $row[3],
		            	'gender'     => $row[4],
		            	'birth_date' => $row[5],
		        	];
		      	})->toArray();
		      
		      	Client::insert($records);
		    });

		    unlink(public_path('temp/dataset.txt'));


            return redirect()->route('index')->with('success', 'Import completed successfully');
		}
		return redirect()->route('index')->with('error', 'please upload txt file');
	}




	public function export(Request $request) {  
		$fileName = 'clients.csv'; 

		$clients = Client::select('*')
			->filterCategory($request->category)
			->filterGender($request->gender)
			->filterBirthday($request->birthdate)
			->filterAge($request->age, $request->age_to)
			->get();   

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = app(Client::class)->getFillable();

        $callback = function() use($clients, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($clients as $client) {
                $row['category']  = $client->category;
                $row['firstname'] = $client->firstname;
                $row['lastname']  = $client->lastname;
                $row['email']     = $client->email;
                $row['gender']    = $client->gender;
                $row['birthDate'] = $client->birth_date;

                fputcsv($file, [
                	$row['category'], 
                	$row['firstname'], 
                	$row['lastname'], 
                	$row['email'], 
                	$row['gender'], 
                	$row['birthDate']
                ]);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
	}

}
